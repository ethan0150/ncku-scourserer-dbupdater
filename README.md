﻿# NCKU-sCourserer-dbUpdater
***NCKU-sCourserer is a project aiming to bring a better course selecting experience. I hope that you won't need to open 50 browser tabs just to find a decent course fitting in your current course schedule anymore.***

This repo is responsible for course info gathering, saving the info into a SQLite DB and publishing it via Gitlab CI/CD artifacts.
## Features(most are WIP)
- multi-department selection
- nckuhub integration
- course schedule importing/planning
- auto course selecting
## How to run this program locally?

**Install these dependencies first**
- Python 3.9.x or newer
- Google Chrome
- After installing Python, run the following command while under a virtualenv to install required Python packages
```sh
pip install -r pyreqs
```
**Finally, simply run:**
```
python main.py
```
