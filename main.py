# -*- coding: UTF-8 -*-
from selenium import webdriver
import chromedriver_autoinstaller
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from sqlalchemy.orm import sessionmaker, declarative_base
from sqlalchemy import create_engine
import time
import random
import codecs
import json
# Base = declarative_base()
from relation import *
# from bs4 import BeautifulSoup


def cut(s):
    return s[s.find(')') + 1:]


def webdriverSetup():

    global driver
    global chrome_option

    chromedriver_autoinstaller.install()
    chrome_option = webdriver.ChromeOptions()
    chrome_option.add_argument("--disable-blink-features=AutomationControlled")
    chrome_option.add_argument("--window-size=1920,1080")
    chrome_option.add_argument("--headless")
# for docker
    chrome_option.add_argument("--no-sandbox")
    chrome_option.add_argument("--disable-setuid-sandbox")
# ---
    driver = webdriver.Chrome(options=chrome_option,service_args=["--verbose", "--log-path=debug.log"])


def scrapeAllCourse():
    driver.get("http://course.ncku.edu.tw/index.php?c=qry11215&m=en_query")
    for i in range(1, 8):
        sel = Select(driver.find_element_by_xpath("//select[@id='sel_wk']"))
        qryBtn = driver.find_element_by_xpath('//button[@onclick="setdata()"]')
        sel.select_by_value(str(i))
        qryBtn.click()
        WebDriverWait(driver, 30).until(EC.invisibility_of_element_located(
            (By.XPATH, '//div[@class="block_circle"]')))
        print("page " + str(i) + " loaded!")
        trList = driver.find_elements_by_xpath('//div[@id="result"]/table/tbody//tr')
        if i < 7:
            time.sleep(0.5 + random.random())


def scrapeDept(sess):
    driver.get("https://course.ncku.edu.tw/index.php?c=qry_all")
    langBtn = driver.find_element_by_xpath(
        """//a[@onclick="javascript:setLang('cht');return false;"]""")
    langBtn.click()
    fp = codecs.open("page.html", "w", "utf-8")
    fp.write(driver.page_source)
    fp.close()
    collList = driver.find_elements_by_xpath(
        '//div[@class="panel-group hidden-xs"]/div')
    for coll in collList:
        deptList = coll.find_elements_by_xpath('.//li[@class="btn_dept"]')
        collName = coll.find_element_by_xpath(
            './/div[@class="panel-heading"]').get_attribute("textContent")
        deptIDList = []
        for dept in deptList:
            deptEntry = cut(dept.get_attribute("textContent"))
            deptID = dept.get_attribute("data-dept")
            deptName = deptEntry[:deptEntry.find(" ")]
            deptAbber = deptEntry[deptEntry.find(" ") + 1:]
            try:
                sess.add(Dept(id=deptID, name=deptName, abber=deptAbber))
            except Exception:
                raise
                sess.rollback()
            deptIDList.append(deptID)
        try:
            sess.add(College(id=collName, depts=json.dumps(deptIDList)))
        except Exception:
            raise
            sess.rollback()


if __name__ == "__main__":
    webdriverSetup()
    engine = create_engine(
        "sqlite+pysqlite:///db.sqlite", echo=True, future=True)
    Base.metadata.create_all(engine)
    Session = sessionmaker(engine)
    with Session() as s:
        scrapeDept(s)
        s.commit()
    driver.quit()
